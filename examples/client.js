'use strict';

// callback url to provide to the PubSubHubbub server
var CALLBACK_URL = 'http://kreata.ee:1337/pubSubHubbub';

var crypto = require('crypto');
var util = require('util');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var createPubSubHubbub = require('../lib/middleware');

// Create  PubSubHubbub subscriber
var client = createPubSubHubbub({
    url: CALLBACK_URL
});

// Parse POST data
app.use(bodyParser.urlencoded({
    extended: false
}));

// Send all incoming traffic to /pubSubHubbub to the PubSubHubbub subscriber
app.use('/pubSubHubbub', client.middleware());

// By default display example page
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/home.html');
});

// Subscribe or unsubscribe to feeds set in the default page
app.post('/', function(req, res) {
    client.setSubscription(req.body.mode, {
        topic: req.body.topic,
        hub: req.body.hub,
        id: crypto.createHash('sha1').update(req.body.topic).digest('hex')
    }, function(err) {
        if (err) {
            res.status(500);
            return res.send(err.message);
        }
        res.status(200);
        res.send('OK');
    });
});

// PubSubHubbub server denied our request
client.on('denied', function(data) {
    console.log('Denied #%s "%s" at "%s"', data.id, data.topic, data.hub);
});

// PubSubHubbub server subscribed our service
client.on('subscribe', function(data) {
    console.log('Subscribed #%s "%s" to "%s"', data.id, data.topic, data.hub);
});

// PubSubHubbub server unsubscribed our service
client.on('unsubscribe', function(data) {
    console.log('Unsubscribed #%s "%s" from "%s"', data.id, data.topic, data.hub);
});

// Some error happened
client.on('error', function(error) {
    console.log('Error');
    console.log(error);
});

// Emits an array of items parsed from the incoming feed
client.on('items', function(items) {
    items.forEach(function(item, i) {
        console.log('Item #%s', i + 1);
        console.log(util.inspect(item, false, 22));
    });
});

// Start express server
app.listen(1337, function() {
    console.log('Server listening');
});