'use strict';

var PubSubHubbub = require('./pubsubhubbub');

module.exports = function(options) {
    return new PubSubHubbub(options);
};