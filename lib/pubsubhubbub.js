'use strict';

var EventEmitter = require('events').EventEmitter;
var util = require('util');
var urllib = require('url');
var crypto = require('crypto');
var request = require('request');
var FeedParser = require('feedparser');

module.exports = PubSubHubbub;

function PubSubHubbub(options) {
    EventEmitter.call(this);

    options = options || {};

    this.secret = options.secret;
    this.url = options.url;
    this.logger = options.logger;
}
util.inherits(PubSubHubbub, EventEmitter);

PubSubHubbub.prototype.log = function() {
    if (this.logger) {
        this.logger.apply(this.logger, [].slice.call(arguments));
    }
};

PubSubHubbub.prototype.subscribe = function(options, callback) {
    this.setSubscription('subscribe', options, callback);
};

PubSubHubbub.prototype.unsubscribe = function(options, callback) {
    this.setSubscription('unsubscribe', options, callback);
};

PubSubHubbub.prototype.setSubscription = function(mode, options, callback) {

    var form, postParams;

    form = {
        'hub.callback': this.formatUrl(this.url, {
            topic: options.topic,
            hub: options.hub,
            id: options.id
        }),
        'hub.mode': mode,
        'hub.topic': options.topic,
        'hub.verify': 'async'
    };

    if (this.secret) {
        // do not use the original secret but a generated one
        form['hub.secret'] = crypto.createHmac('sha1', this.secret).update(options.topic).digest('hex');
    }

    postParams = {
        url: options.hub,
        form: form,
        encoding: 'utf-8'
    };

    if (options.request) {
        Object.keys(options.request).forEach(function(key) {
            postParams[key] = options[key];
        });
    }

    request.post(postParams, function(err, response, responseBody) {

        if (err) {
            err.pubsub = {
                topic: options.topic,
                hub: options.hub,
                id: options.id,
                mode: mode
            };
            return callback(err);
        }

        if (response.statusCode !== 202 && response.statusCode !== 204) {
            err = new Error('Invalid response status ' + response.statusCode);
            err.pubsub = {
                topic: options.topic,
                hub: options.hub,
                id: options.id,
                mode: mode,
                code: response.statusCode,
                body: responseBody
            };

            return callback(err);
        }

        return callback(null, true);
    });
};

PubSubHubbub.prototype.middleware = function() {
    return this.requestHandler.bind(this);
};

PubSubHubbub.prototype.requestHandler = function(req, res, next) {
    switch (req.method) {
        case 'GET':
            return this.getHandler(req, res, next);
        case 'POST':
            return this.postHandler(req, res, next);
        default:
            next(new Error('Method Not Allowed'));
    }
};

PubSubHubbub.prototype.getHandler = function(req, res, next) {
    var data;

    // Does not seem to be a valid PubSubHubbub request
    if (!req.query['hub.topic'] || !req.query['hub.mode']) {
        this.logger('Invalid GET request, no mode or topic');
        return next(new Error('Bad Request'));
    }

    switch (req.query['hub.mode']) {
        case 'denied':
            data = {
                topic: req.query['hub.topic'],
                hub: req.query.hub,
                id: req.query.id
            };
            res.status(200);
            res.set('Content-Type', 'text/plain');
            res.send(req.query['hub.challenge'] || 'ok');
            this.logger('Denied %s by %s', req.query['hub.topic'], req.query.hub);
            break;

        case 'subscribe':
        case 'unsubscribe':
            data = {
                topic: req.query['hub.topic'],
                hub: req.query.hub,
                id: req.query.id
            };

            if (req.query['hub.lease_seconds'] && Number(req.query['hub.lease_seconds'])) {
                data.lease = new Date(Date.now() + req.query['hub.lease_seconds'] * 1000);
            }

            res.statusCode = 200;
            res.set('Content-Type', 'text/plain');
            res.send(req.query['hub.challenge']);
            this.logger('Subscription updated for %s at %s (%s)', req.query['hub.topic'], req.query.hub, req.query['hub.mode']);
            break;

        default:
            // Not a valid mode
            return next(new Error('Forbidden'));
    }

    // Emit subscription information
    this.emit(req.query['hub.mode'], data);
};

PubSubHubbub.prototype.postHandler = function(req, res, next) {
    var topic = req.query.topic;
    var hub = req.query.hub;
    var id = req.query.id;
    var signatureParts, algo, signature, hmac;
    var that = this;

    if (!topic) {
        this.logger('Invalid POST request, no topic');
        return next(new Error('Bad Request'));
    }

    // Hub must notify with signature header if secret specified.
    if (this.secret && !req.headers['x-hub-signature']) {
        this.logger('Invalid POST request, missing signature');
        return next(new Error('Forbidden'));
    }

    if (this.secret) {
        signatureParts = req.headers['x-hub-signature'].split('=');
        algo = (signatureParts.shift() || '').toLowerCase();
        signature = (signatureParts.pop() || '').toLowerCase();

        try {
            hmac = crypto.createHmac(algo, crypto.createHmac('sha1', this.secret).update(topic).digest('hex'));
        } catch (E) {
            return next(new Error('Forbidden'));
        }
    }

    var feedparser = new FeedParser({
        addmeta: true,
        feedurl: topic
    });

    if (hmac) {
        req.pipe(hmac, {
            end: false
        });
    }

    var items = [];

    this.logger('Parsing incoming stream for %s', topic);
    req.pipe(feedparser);

    req.on('error', function(err) {
        this.emit('error', err);
    }.bind(this));

    feedparser.on('error', function(err) {
        this.emit('error', err);
    }.bind(this));

    feedparser.on('readable', function() {
        var stream = this;
        var item;

        while ((item = stream.read())) {
            if (item) {
                item.topic = topic;
                item.hub = hub;
                item.id = id;
                items.unshift(item);
            }
        }
    });

    feedparser.on('end', function() {
        var signed = true;

        if (this.secret && typeof hmac !== 'string') {
            hmac = hmac.digest('hex').toLowerCase();
            if (hmac !== signature) {
                signed = false;
            }
        }

        that.emit('items', items, signed);
    }.bind(this));

    req.on('end', (function() {
        if (this.secret && typeof hmac !== 'string') {
            hmac = hmac.digest('hex').toLowerCase();
        }
        if (this.secret && hmac !== signature) {
            res.statusCode = 202;
        } else {
            res.statusCode = 204;
        }

        res.set('Content-Type', 'text/plain; charset=utf-8');
        res.send('');
    }).bind(this));
};

PubSubHubbub.prototype.formatUrl = function(url, query) {
    query = query || {};
    var parsed = urllib.parse(url, true);
    delete parsed.search;
    Object.keys(query).forEach(function(key) {
        if (query[key]) {
            parsed.query[key] = query[key];
        }
    });
    return urllib.format(parsed);
};